﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Division table; enter a number and console will show division from 1 to 12 for given number");
            var input = double.Parse(Console.ReadLine());
            var counter = 12;
            var i = 0;
            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"{input} / {a} = {Math.Round(input / a,2)}");
            }
        }
    }
}
