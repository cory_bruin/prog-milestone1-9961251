﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name");
            var name = Console.ReadLine();
            Console.WriteLine("Please enter your age");
            var age = int.Parse(Console.ReadLine());
            Console.WriteLine("Your name is {0} and your age is {1}", name, age);
            Console.WriteLine($"Your name is {name} and your age is {age}");
            Console.WriteLine("Your name is " + name + " and your age is " + age);
        }
    }
}
