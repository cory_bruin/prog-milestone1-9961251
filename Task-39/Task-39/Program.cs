﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_39
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = new string[12] {"January","February","March","April","May","June","July","August","September","October","November","December"};
            var days = new int[12] {31,28,31,30,31,30,31,31,30,31,30,31};
            var i = 0;
            Console.WriteLine("Assuming the 1st of the month is a Monday;");
            while (i < 12)
            {
                if (days[i] > 29)
                {
                    Console.WriteLine($"{month[i]} has 5 Mondays in a month");
                    i++;
                }
                else
                {
                    Console.WriteLine($"{month[i]} has 4 Mondays in a month");
                    i++;
                }
            }
        }
    }
}
