﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 3 words");
            var input = Console.ReadLine().Split(' ');
            var i = 0;
            while (i < input.Length)
            {
                Console.WriteLine(input[i]);
                i++;
            }
        }
    }
}
