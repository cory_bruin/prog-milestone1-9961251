﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new int[7] { 34, 45, 21, 44, 67, 88, 86 };
            var i = 0;
            var counter = 7;
            var even = new List<int> { };
            while (counter > i)
            {
                if (numbers[i] % 2 == 0)
                {
                    even.Add(numbers[i]);
                    i++;
                }
                else { i++; }
            }
            var x = 0;
            while (even.Count > x)
            {
                Console.WriteLine($"Result {x + 1}/{even.Count}: {even[x]}");
                x++;
            }
            
        }
    }
}
