﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many working days are in a number of weeks - please enter a number of weeks you would like converted");
            var input = int.Parse(Console.ReadLine());
            Console.WriteLine($"There are {input * 5} working days in {input} weeks.");
        }
    }
}
