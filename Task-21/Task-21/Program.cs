﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            dictionary.Add("January", 31);
            dictionary.Add("February", 28);
            dictionary.Add("March", 31);
            dictionary.Add("April", 30);
            dictionary.Add("May", 31);
            dictionary.Add("June", 30);
            dictionary.Add("July", 31);
            dictionary.Add("August", 31);
            dictionary.Add("September", 30);
            dictionary.Add("October", 31);
            dictionary.Add("November", 30);
            dictionary.Add("December", 31);
            Console.WriteLine("Months that have 31 days");
            foreach (KeyValuePair<string, int> pair in dictionary)
            {
                if (pair.Value == 31)
                {
                    Console.WriteLine(pair.Key);
                }
            }

        }
    }
}
