﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("5 number calculator - enter in 5 numbers and console will calculate the answer for you.");
            var i = 0;
            var number = 5;
            var input = new List<int> { };
            var total = 0;
            for (i = 0; number > i; i++)
            {
                Console.WriteLine($"Entry {i + 1}/5: Please enter a number");
                input.Add(int.Parse(Console.ReadLine()));
                total = input[i] + total;
            }
            Console.WriteLine($"The answer to {input[0]} + {input[1]} + {input[2]} + {input[3]} + {input[4]} = {total}");
        }
    }
}
