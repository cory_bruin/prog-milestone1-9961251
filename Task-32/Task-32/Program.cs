﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = new string[7, 2] { { "Monday", "1" }, { "Tuesday", "2" }, { "Wednesday", "3" }, { "Thursday", "4" }, { "Friday", "5" }, { "Saturday", "6" }, { "Sunday", "7" } };
            var i = 0;
            while (i < 7)
                {
                Console.WriteLine($"{days[i, 0]} is day {days[i, 1]} of the week");
                i++;
                }
        }
    }
}
