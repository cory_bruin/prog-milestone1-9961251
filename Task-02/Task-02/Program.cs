﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your birth month");
            var month = Console.ReadLine();
            Console.WriteLine("Please enter your birth day");
            var day = int.Parse(Console.ReadLine());
            if (day >= 1 && day <= 9 || day >= 21 && day <= 31)
            {
                if ((day % 10) == 1) { Console.WriteLine($"You were born on the {day}st of {month}"); };
                if ((day % 10) == 2) { Console.WriteLine($"You were born on the {day}nd of {month}"); };
                if ((day % 10) == 3) { Console.WriteLine($"You were born on the {day}rd of {month}"); };
                if ((day % 10) >= 4) { Console.WriteLine($"You were born on the {day}th of {month}"); };
            }
            else if (day >= 10 && day <= 20)
            {
                Console.WriteLine($"You were born on the {day}th of {month}");
            }
        }
    }
}
