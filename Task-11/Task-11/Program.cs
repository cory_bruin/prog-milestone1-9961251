﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a year to see if that year was a leap year");
            var year = int.Parse(Console.ReadLine());
            if (year % 4 != 0)
            {
                Console.WriteLine($"The year {year} is not a leap year");
            }
            else if (year % 4 == 0 && year % 100 != 0)
            {
                Console.WriteLine($"The Year {year} is a leap year");
            }
            else if (year % 100 == 0 && year % 400 == 0)
            {
                Console.WriteLine($"The year {year} is a leap year");
            }
            else
            {
                Console.WriteLine($"The year {year} is not a leap year");
            }
        }
    }
}
