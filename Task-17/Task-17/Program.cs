﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 3;
            var names = new List<Tuple<String, int>>();
            Console.WriteLine("Creating a list that holds the name and age of 3 people");
            for (i = 0; i < counter; i++)
            {
                Console.WriteLine("Please enter a name");
                var name1 = Console.ReadLine();
                Console.WriteLine($"How old is {name1}?");
                var age1 = int.Parse(Console.ReadLine());
                names.Add(Tuple.Create(name1, age1));
            }
            for (i = 0; i < counter; i++)
            {
                Console.WriteLine($"Entry {i + 1}: {names[i].Item1} is {names[i].Item2} years old.");         
            }
        }
    }
}
