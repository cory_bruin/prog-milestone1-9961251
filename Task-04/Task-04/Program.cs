﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
            Console.WriteLine("Would you like to convert from Fahrenheit to Celsius or Celsius to Fahrenheit");
            Console.WriteLine("Press 1 for Fahrenheit to Celsius");
            Console.WriteLine("Press 2 for Celsius to Fahrenheit");
            var decision = int.Parse(Console.ReadLine());
            if (decision == 1)
            {
                Console.WriteLine("Please enter the temperature in Fahrenheit you would like converted to Celsius");
                var temp = int.Parse(Console.ReadLine());
                Console.WriteLine($"{temp} degrees Fahrenheit is {(temp - 32) * 0.5556} degrees in Celsius");
            }
            else if (decision == 2)
            {
                Console.WriteLine("Please enter the temperature in Celsius you would like converted to Fahrenheit");
                var temp = int.Parse(Console.ReadLine());
                Console.WriteLine($"{temp} degrees Celsius is {(temp * 1.8) + 32} degrees in Fahrenheit");
            }
            else
            {
                Console.WriteLine("Invalid Number, try again");
                Console.WriteLine();
                goto Start;
            }
        }
    }
}
