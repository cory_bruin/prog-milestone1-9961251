﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 3 whole numbers and the console will perform 5 math equations");
            var input = Console.ReadLine();
            var numbers = input.Split(' ');
            int[] numbers1 = Array.ConvertAll(numbers, int.Parse);
            Console.WriteLine($"{numbers1[0]} + {numbers1[1]} = {numbers1[0] + numbers1[1]}");
            Console.WriteLine($"{numbers1[2]} - {numbers1[1]} = {numbers1[2] - numbers1[1]}");
            Console.WriteLine($"{numbers1[0]} x {numbers1[2]} = {numbers1[0] * numbers1[2]}");
            Console.WriteLine($"{numbers1[0]} + {numbers1[1]} + {numbers1[2]} = {numbers1[0] + numbers1[1] + numbers1[2]}");
            Console.WriteLine($"{numbers1[0]} x {numbers1[1]} + {numbers1[2]} = {numbers1[0] * numbers1[1] + numbers1[2]}");

        }
    }
}
