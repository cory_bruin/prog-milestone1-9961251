﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 2 numbers");
            var input = Console.ReadLine().Split(' ');
            Console.WriteLine($"This is what happens when you add 2 numbers as a string: {input[0]  + input[1]}");
            Console.WriteLine($"This is what happens when you add 2 numbers as an int: {int.Parse(input[0]) + int.Parse(input[1])}");

        }
    }
}
