﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;
            var counter = 3;
            var names = new List<Tuple<String, String, String>>();
            Console.WriteLine("Creating a list that holds the name, month and birthday of 3 people");
            for (i = 0; i < counter; i++)
            {
                Console.WriteLine("Please enter a name, month and birthday(format: 'Cory February 12')");
                var info = Console.ReadLine();
                var input = info.Split(' ');
                names.Add(Tuple.Create(input[0], input[1], input[2]));
            }
            for (i = 0; i < counter; i++)
            {
                Console.WriteLine($"Entry {i + 1}: {names[i].Item1} was born on {names[i].Item2} {names[i].Item3}.");
            }
        }
    }
}
