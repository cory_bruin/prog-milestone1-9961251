﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            int vegecount = 0;
            int fruitcount = 0;
            List<string> fruitlist = new List<string>();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("Onion", "Vegetable");
            dictionary.Add("Cabbage", "Vegetable");
            dictionary.Add("Corn", "Vegetable");
            dictionary.Add("Cucumber", "Vegetable");
            dictionary.Add("Pumpkin", "Vegetable");
            dictionary.Add("Apple", "Fruit");
            dictionary.Add("Orange", "Fruit");
            dictionary.Add("Peach", "Fruit");
            dictionary.Add("Pineapple", "Fruit");
            dictionary.Add("Kiwifruit", "Fruit");
            dictionary.Add("Mango", "Fruit");
            dictionary.Add("Lemon", "Fruit");
            foreach (KeyValuePair<string, string> pair in dictionary)
            {
                if (pair.Value == "Fruit")
                {
                    fruitlist.Add(pair.Key);
                    fruitcount = ++fruitcount;
                }
                else
                {
                    vegecount = ++vegecount;
                }
            }
            Console.WriteLine($"Count of Vegetables in Dictionary: {vegecount}");
            Console.WriteLine($"Count of Fruits in Dictionary: {fruitcount}");
            Console.WriteLine($"List of all the fruit: {string.Join(", ", fruitlist)}");
        }
    }
}
